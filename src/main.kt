fun main() {
    println(davaleba1(arrayOf(1,2,3,4,9)))
    println(davaleba2("This is not a palindrome"))
    println(davaleba2("Reviver"))


}

fun  davaleba1(numbers: Array<Int>): Int { // დავალებაში ეწერა მთელი ტიპიო თორემ მე დუბლს დავაბრუნებინებდი საშუალო ხშირ შემთხვევაში წილადი იქნება
    var sum = 0
    var count = 0
    for (i in numbers.indices step 2){
        sum += numbers[i]
        count ++
    }
    return sum/count
}

fun davaleba2(word: String): Boolean {

    var reverseString = ""

    for (i in (word.length - 1) downTo 0) {
        reverseString += word[i]
    }

    return word.equals(reverseString, ignoreCase = true)
}